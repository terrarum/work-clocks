# work-clocks

Issues at https://gitlab.com/terrarum/work-clocks/-/issues.

## Alarm Behaviours
Ready
    Play button -> Counting
    Display default time
    Flash disabled
    Tone disabled
Counting
    Pause button -> Paused
    Display counting time
    Flash disabled
    Tone disabled
Paused
    Resume button -> Counting
    Reset button -> Ready
    Display paused time
    Flash enabled
    Tone disabled
Complete
    Reset button -> Ready
    Display zeroes
    Tone enabled
    Flash enabled

Reset button should reset to Ready state

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
